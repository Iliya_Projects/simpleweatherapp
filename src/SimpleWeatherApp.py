'''
Created on Feb 13, 2016

powerd by: http://www.worldweatheronline.com

@author: iliya
'''

import urllib2
from sys import argv
import json
class SimpleWeatherApp:

    freeWorldWeatherOnlineKey = "you can get your free key on http://www.worldweatheronline.com"
    freeHttpsURL = "https://api.worldweatheronline.com/free/v2/ski.ashx"
   

    def getWeather(self):        
        city = self.inputCity()
        queryUrl = self.freeHttpsURL + "?q=" + city +"&key=" + self.freeWorldWeatherOnlineKey + "&format=json"
        getFile =  urllib2.urlopen(queryUrl)
        data = json.load(getFile)
        return data
    
    def inputCity(self):
        
        while 1:
            userInput = raw_input("Please enter the name of the city:")
            if self.containsDigits(userInput) == False:
               userInput = userInput.replace(" ", "_")
               break
            else:
                print "The city name should not contain a digit."
            
        return userInput
    
    def containsDigits(self,input):
       return any(char.isdigit() for char in input)
        
        