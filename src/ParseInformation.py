'''
Created on Feb 13, 2016

Use this to parse the JSON file and get the information about the weather that is needed.
@author: iliya
'''
from src.SimpleWeatherApp import SimpleWeatherApp
import json
class ParseInformation:
    weatherFile = 0
    requestError = 0
    def __init__(self):
        weatherApp = SimpleWeatherApp()
        self.weatherFile = weatherApp.getWeather()
       
    def hasErrors(self):
        if "error" in self.weatherFile["data"]:
            self.requestError =  self.weatherFile["data"]["error"][0]["msg"]   
            return True
        else:
           return False
          
    def returnFileInfo(self):
        return self.weatherFile 
    
    def getCityName(self):
        return self.weatherFile["data"]["request"][0]["query"]
  
    def getDate(self): 
        return self.weatherFile["data"]["weather"][0]["date"]
    
    def getCurrentMaxTemp(self):
        return self.weatherFile["data"]["weather"][0]["bottom"][0][ "maxtempC"]
    
    def getCurrentMinTemp(self):
        return self.weatherFile["data"]["weather"][0]["bottom"][0][ "mintempC"]