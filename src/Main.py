'''
Created on Feb 13, 2016

powerd by http://www.worldweatheronline.com/api/

@author: iliya
'''
from src import ParseInformation


parse = ParseInformation.ParseInformation()

if parse.hasErrors() == True:
    print parse.requestError
else:    
    city = parse.getCityName()
    date = parse.getDate()
    minTemp = parse.getCurrentMinTemp()
    maxTemp = parse.getCurrentMaxTemp()
    print "For the city %s on the %s the maximal temperature will be %sC and the minimal %sC." % (city, date, maxTemp, minTemp)