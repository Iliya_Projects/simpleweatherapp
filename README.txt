This is a simple weather script. It gets the current weather for a given city and prints it on the console. The used weather API is http://www.worldweatheronline.com and is the free version. If you would like to use this simple script you will have to register and get yourself a free API key.  

For more information about the API visit: http://www.worldweatheronline.com/api/